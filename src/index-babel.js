/* Thank you Wes Bos for this lovely code from his cssgrid.io course. Go check it out if you want to learn CSS Grid. It's been modified for my specific use. */
const logoGallery = document.querySelector('#logo-section .gallery');
const shirtGallery = document.querySelector('#shirt-section .gallery');
const webGallery = document.querySelector('#web-section .gallery');
const overlay = document.querySelector('.overlay');
const overlayImage = overlay.querySelector('img');


    function generateHTML(nm) {
      return `
        <div class="item">
          <img src="/${nm}.jpg">
          <div class="item__overlay">
            <button>Zoom</button>
          </div>
        </div>
      `;
    }

    function handleClick(e) {
      const src = e.currentTarget.querySelector('img').src;
      overlayImage.src = src;
      overlay.classList.add('open');
    }


    function close() {
      overlay.classList.remove('open');
    }
const logoList = ['logo1','logo2','logo3','logo4'];
const shirtList = ['shirt1','shirt2','shirt3','shirt4','shirt5'];
const webList = ['web1'];

const logoHtml = logoList.map(generateHTML).join('');
    logoGallery.innerHTML = logoHtml;

const shirtHtml = shirtList.map(generateHTML).join('');
    shirtGallery.innerHTML = shirtHtml;

const webHtml = webList.map(generateHTML).join('');
    webGallery.innerHTML = webHtml;

// Give each image item a 'click' event    
const items = document.querySelectorAll('.item');
items.forEach(item => item.addEventListener('click', handleClick));

// Give each overlay item a close event
const overlayClose = overlay.querySelector('.close');
overlayClose.addEventListener('click', close);
