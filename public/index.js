'use strict';

/* Thank you Wes Bos for this lovely code from his cssgrid.io course. Go check it out if you want to learn CSS Grid. It's been modified for my specific use. */
var logoGallery = document.querySelector('#logo-gallery');
var shirtGallery = document.querySelector('#shirt-gallery');
var webGallery = document.querySelector('#web-gallery');
var overlay = document.querySelector('.overlay');
var overlayImage = overlay.querySelector('img');

function generateHTML(nm) {
  return `<div class="item">
            <img src="${nm}.jpg">
              <div class="item__overlay">
                <button>Zoom</button>
              </div>
          </div>`;
}

function handleClick(e) {
  var src = e.currentTarget.querySelector('img').src;
  overlayImage.src = src;
  overlay.classList.add('open');
}

function close() {
  overlay.classList.remove('open');
}
var logoList = ['logo1', 'logo2', 'logo3', 'logo4'];
var shirtList = ['shirt1', 'shirt2', 'shirt3', 'shirt4', 'shirt5'];
var webList = ['web1'];

var logoHtml = logoList.map(generateHTML).join('');
var logoTitle = `<div id="logo-title" class="title"><a href="#menu-sec">Logos</a></div>`
logoGallery.innerHTML = logoTitle
logoGallery.innerHTML += logoHtml;
var shirtHtml = shirtList.map(generateHTML).join('');
var shirtTitle = `<div id="shirt-title" class="title"><a href="#menu-sec">Shirts</a></div>`
logoGallery.innerHTML += shirtTitle + shirtHtml;

var webHtml = webList.map(generateHTML).join('');
var webTitle = `<div id="web-title" class="title"><a href="#menu-sec">Web</a></div>`
logoGallery.innerHTML += webTitle + webHtml;

// Give each image item a 'click' event    
var items = document.querySelectorAll('.item');
items.forEach(function (item) {
  return item.addEventListener('click', handleClick);
});

// Give each overlay item a close event
var overlayClose = overlay.querySelector('.close');
overlayClose.addEventListener('click', close);
